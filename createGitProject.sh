#!/bin/bash

while [[ $# -gt 0 ]]; do
  case $1 in
    -d|--directory) # ABSOLUTE path for directory for GIT repo
      DIRECTORY="$2"
      shift # past argument
      shift # past value
      ;;
    -n|--name) # ABSOLUTE path for directory for GIT repo
      NAME="$2"
      shift # past argument
      shift # past value
      ;;
    -o|--origin) # Origin path for online repo (links local repo to online version)
      ORIGIN="$2"
      shift # past argument
      shift # past value
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

DATETIME=$(date +%F)

# Check if Origin has been provided (links locl repo to online repo)
if [ -z $ORIGIN ]
then
    echo "Git Origin '-o' must be provided to create a local repo"
    exit
fi

CWD=$PWD

# Ensure the repo does not already exist locally
if [ ! -d $DIRECTORY ]; then
    mkdir -p $DIRECTORY
else
    echo "$DIRECTORY already exists!"
    exit
fi

## Create base files/dirs within $DIRECTORY
mkdir -p $DIRECTORY/data
touch $DIRECTORY/data/.gitkeep

mkdir -p $DIRECTORY/tmp
touch $DIRECTORY/tmp/.gitkeep

mkdir -p $DIRECTORY/out
touch $DIRECTORY/out/.gitkeep

# Populate README.md Base Template
touch $DIRECTORY/README.md

echo -e "# Project: $DIRECTORY\n" >> $DIRECTORY/README.md
echo -e "**Version**: 0.0.0\n" >> $DIRECTORY/README.md
echo -e "**Date Initiated**: $DATETIME\n" >> $DIRECTORY/README.md
echo -e "**Developer**: $NAME\n" >> $DIRECTORY/README.md

echo -e "## Description\n" >> $DIRECTORY/README.md

echo -e "Briefly describe your bioinformatics project. Include information on what the project does, the problem it solves, and why it is useful. Mention any unique features or approaches your project takes.\n" >> $DIRECTORY/README.md

echo -e "## Project Structure\n" >> $DIRECTORY/README.md

echo -e "The structure of this project can be outlined by 4 core sections: \`rootDir\`,\`data\`,\`out\`,\`tmp\`.\n" >> $DIRECTORY/README.md

echo -e "|Section| Description|" >> $DIRECTORY/README.md
echo -e "|---|---|" >> $DIRECTORY/README.md
echo -e "|rootDir|This is the primary directory of the project and will function as the main area of development. It contains at minimum the project README, the directories mentioned below, and scripts created during development|" >> $DIRECTORY/README.md
echo -e "|data|(NOT TRACKED) Contains input data for use by scripts. Data held here will often speed up analysis as scripts will not need to access network shares|" >> $DIRECTORY/README.md
echo -e "|out|(NOT TRACKED) Functions as a LOCAL location to store outputs of analysis|" >> $DIRECTORY/README.md
echo -e "|tmp|(NOT TRACKED) Ideal for storing intermediate files used during analysis. i.e. The use of temp matrix files rather than storing ALL data in memory durign analysis|" >> $DIRECTORY/README.md
echo -e "" >> $DIRECTORY/README.md

echo -e "## Getting Started\n" >> $DIRECTORY/README.md

echo -e "### Dependencies\n" >> $DIRECTORY/README.md

echo -e "List any libraries, frameworks, or tools that your project depends on. Specify versions if necessary and any other system requirements (e.g., Python version, R version, operating system compatibility).\n" >> $DIRECTORY/README.md

echo -e "### Installing\n" >> $DIRECTORY/README.md

echo -e "Provide step-by-step instructions on how to get a development environment running, including how to install any dependencies. Be specific to make this process as easy as possible for newcomers.\n" >> $DIRECTORY/README.md

echo "\`\`\`bash" >> $DIRECTORY/README.md
echo -e "git clone ${ORIGIN}" >> $DIRECTORY/README.md
echo -e "cd ${DIRECTORY}" >> $DIRECTORY/README.md
echo -e "pip install -r requirements.txt" >> $DIRECTORY/README.md
echo "\`\`\`" >> $DIRECTORY/README.md

echo -e "### Data Preparation\n" >> $DIRECTORY/README.md

echo -e "If your project requires specific data files or formats, describe how to prepare the input data. Provide examples or links to where users can find the necessary datasets.\n" >> $DIRECTORY/README.md

echo -e "## Usage\n" >> $DIRECTORY/README.md

echo -e "Offer detailed examples on how to run your software, including any command line arguments or options. Break down the steps for common workflows or tasks your tool is designed for.\n" >> $DIRECTORY/README.md

echo "\`\`\`bash" >> $DIRECTORY/README.md
echo -e "python your_script.py -i input.fasta -o output.json --option1 --option2" >> $DIRECTORY/README.md
echo "\`\`\`" >> $DIRECTORY/README.md

echo -e "### Advanced Options\n" >> $DIRECTORY/README.md

echo -e "If your tool has advanced features or configurations, document them here. Explain what each option does and when it might be beneficial to use them.\n" >> $DIRECTORY/README.md

echo -e "## Contributing\n" >> $DIRECTORY/README.md

echo -e "Encourage contributions by explaining how others can contribute to your project. Include instructions for submitting bugs, feature requests, and pull requests.\n" >> $DIRECTORY/README.md

echo -e "|Name|Role|" >> $DIRECTORY/README.md
echo -e "|---|---|" >> $DIRECTORY/README.md
echo -e "|$NAME|Developer|" >> $DIRECTORY/README.md
echo -e "|Andrew Smith|Lead Bioinformatician|" >> $DIRECTORY/README.md
echo -e "|Dr Kelly Williams|Project Lead|" >> $DIRECTORY/README.md
echo -e "" >> $DIRECTORY/README.md

echo -e "## License\n" >> $DIRECTORY/README.md

echo -e "Specify the license under which your project is released. This informs users how they are allowed to use and distribute your software.\n" >> $DIRECTORY/README.md

echo "\`\`\`csharp" >> $DIRECTORY/README.md
echo -e "This project is licensed under the MIT License - see the LICENSE.md file for details" >> $DIRECTORY/README.md
echo "\`\`\`" >> $DIRECTORY/README.md

echo -e "## Citation\n" >> $DIRECTORY/README.md

echo -e "If you have published a paper describing your project, provide a citation in BibTeX or another preferred format, so users can easily reference your work.\n" >> $DIRECTORY/README.md

echo "\`\`\`bibtex" >> $DIRECTORY/README.md
echo -e "@article{YourName2023YourProject," >> $DIRECTORY/README.md
echo -e "  title={Your Project Title}," >> $DIRECTORY/README.md
echo -e "  author={Your Name and Collaborator's Name}," >> $DIRECTORY/README.md
echo -e "  journal={Journal Name}," >> $DIRECTORY/README.md
echo -e "  year={2023}," >> $DIRECTORY/README.md
echo -e "  volume={xx}," >> $DIRECTORY/README.md
echo -e "  pages={xxx-xxx}" >> $DIRECTORY/README.md
echo -e "}" >> $DIRECTORY/README.md
echo "\`\`\`" >> $DIRECTORY/README.md

echo -e "## Acknowledgments\n" >> $DIRECTORY/README.md

echo -e "Mention anyone or any organization that helped your project or provided data. This can include funding sources, advisory support, or data providers.\n" >> $DIRECTORY/README.md

# Create Base Changelog.md

echo "# Changelog" >> $DIRECTORY/CHANGELOG.md

echo "All notable changes to this project will be documented in this file." >> $DIRECTORY/CHANGELOG.md

echo "The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)," >> $DIRECTORY/CHANGELOG.md
echo "and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)." >> $DIRECTORY/CHANGELOG.md

echo "Change options:" >> $DIRECTORY/CHANGELOG.md

echo "| Option   | Description                                |" >> $DIRECTORY/CHANGELOG.md
echo "| -------- | ------------------------------------------ |" >> $DIRECTORY/CHANGELOG.md
echo "| Fixed    | Fixing issues in the project                  |" >> $DIRECTORY/CHANGELOG.md
echo "| Added    | Added an additional feature                |" >> $DIRECTORY/CHANGELOG.md
echo "| Changed  | Changed logic or how the project operates |" >> $DIRECTORY/CHANGELOG.md
echo "| Security | Altered security for the project          |" >> $DIRECTORY/CHANGELOG.md

echo "## [v0.0.0] - TBC" >> $DIRECTORY/CHANGELOG.md

echo "### Added" >> $DIRECTORY/CHANGELOG.md

# Navigate to Git Repo/Dir
cd $DIRECTORY

# Initiate Git Repo with empty source Dirs
git init
git remote add origin $ORIGIN
git add .
git commit -m "Initial Commit"
git push --set-upstream origin master

# Add .gitignore file (Will ensure ALL files inside these directories will NOT be uploaded to git)
echo "data/" >> .gitignore
echo "tmp/" >> .gitignore
echo "out/" >> .gitignore

git add .gitignore
git commit -m "Added base .gitignore"