# Create Git Repo

**Date Last Updated**: 2024-04-05

**Developer**: Andrew Smith (Lead Bioinformaician)

## Description

The script contained in this repo is designed to help researched at the MQ-MND centre setup their Git respositores. It aims to automate much of the underlying processes that would typically be carried out manually. Thus ensuring repeatability across researchers, improviing familiarity and readability, and as a result ensuring smoother collaboration due to a standarised project structure for code based projects